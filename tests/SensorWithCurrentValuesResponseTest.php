<?php

namespace App\Tests;

use App\Entity\Quantity;
use App\Entity\Sensor;
use App\Enums\Trend;
use App\Response\DewpointSensorCurrentValuesDecorator;
use App\Response\QuantityCurrentValueWithResponse;
use App\Response\SensorCurrentValueWithTrendResponse;
use App\Service\SensorService;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SensorWithCurrentValuesResponseTest extends KernelTestCase
{
    public function testSensorWithTemperatureAndHumidityShouldReturnAlsoDewpoint(): void
    {

        self::bootKernel();
        $container = static::getContainer();

        $sensorService = $container->get(SensorService::class);

        $humidity = QuantityTestFactory::makeHumidity();
        $humidityCurrentValueWithResponse = new QuantityCurrentValueWithResponse();
        $humidityCurrentValueWithResponse
            ->setQuantity($humidity)
            ->setTrend(Trend::STABLE)
            ->setCurrent(82)
            ->setTrendValues([86,85,88])
        ;

        $temperature = QuantityTestFactory::makeTemperature();
        $temperatureCurrentValueWithResponse = new QuantityCurrentValueWithResponse();
        $temperatureCurrentValueWithResponse
            ->setQuantity($temperature)
            ->setTrend(Trend::STABLE)
            ->setCurrent(26.4)
            ->setTrendValues([26.4,26.8,26.3])
        ;

        $sensorWithTemperatureAndHumidityResponse = new SensorCurrentValueWithTrendResponse();
        $sensorWithTemperatureAndHumidityResponse
            ->addQuantityResponse($humidityCurrentValueWithResponse)
            // ->addQuantityResponse($temperatureCurrentValueWithResponse)
        ;
        
        $dewpointResponseDecorator1 = new DewpointSensorCurrentValuesDecorator($sensorWithTemperatureAndHumidityResponse, $sensorService);
        $arrayResponse = $dewpointResponseDecorator1->toArray();

        $this->assertArrayHasKey('quantity', end($arrayResponse));
        
        $this->assertFalse(end($arrayResponse)['quantity']['name']=='dewpoint');
    
        $sensorWithTemperatureAndHumidityResponse
            ->addQuantityResponse($temperatureCurrentValueWithResponse)
        ;

        $dewpointResponseDecorator2 = new DewpointSensorCurrentValuesDecorator($sensorWithTemperatureAndHumidityResponse, $sensorService);
        $arrayResponse = $dewpointResponseDecorator2->toArray();

        $this->assertTrue(end($arrayResponse)['quantity']['name']=='dewpoint');
    }
}
