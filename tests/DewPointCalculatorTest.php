<?php

namespace App\Tests;

use App\Service\SensorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DewPointCalculatorTest extends KernelTestCase
{
    public function testSomething(): void
    {
        $kernel = self::bootKernel();
        $sensorService = static::getContainer()->get(SensorService::class);

        //temp, rh, dewpoint
        $testCases = [
            [25, 60, 16.7],
            [10, 50, -0],
            [30, 70, 23.9],
            [-5, 80, -8.0],
            [20, 40, 5.9],
        ];

        foreach ($testCases as $testCase) {
            $dewPoint = $sensorService->calculateDewPoint(
                $testCase[0],
                $testCase[1]
            );

            $this->assertEquals($testCase[2], round($dewPoint, 1));
        }
    }
}
