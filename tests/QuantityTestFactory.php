<?php 

namespace App\Tests;
use App\Entity\Quantity;

class QuantityTestFactory {

    public static function makeHumidity()
    {
        $quantity = new Quantity();
        $quantity
            ->setEnabled(true)
            ->setName('humidity')
            ->setUnit('%')
            ->setParameterName('humidity')
        ;

        return $quantity;
    }

    public static function makeTemperature()
    {
        $quantity = new Quantity();
        $quantity
            ->setEnabled(true)
            ->setName('temperature')
            ->setUnit('°C')
            ->setParameterName('temperature')
        ;

        return $quantity;
    }

}

?>