<?php

namespace App\Service;

use App\Entity\Quantity;
use App\Entity\Sensor;
use App\Entity\SensorData;
use App\Enums\Trend;
use App\Exceptions\SensorNotFoundException;
use App\Filters\SensorFilterQuery;
use App\Factory\SensorDataFactory;
use App\Repository\SensorDataRepository;
use App\Repository\SensorRepository;
use Doctrine\ORM\EntityManagerInterface;

class SensorService
{
    // @var EntityManagerInterface
    private $em;

    // @var SensorRepository
    private $sensorRepository;

    // @var QuantityService
    private $qs;

    // @var SensorDataRepository
    private $sensorDataRepository;

    // @var float
    private $stableTrendDiff;

    public function __construct(
        $stableTrendDiff,
        EntityManagerInterface $em,
        SensorRepository $sensorRepository,
        QuantityService $qs,
        SensorDataRepository $sensorDataRepository
    ) {
        $this->em = $em;
        $this->sensorRepository = $sensorRepository;
        $this->qs = $qs;
        $this->sensorDataRepository = $sensorDataRepository;
        $this->stableTrendDiff = $stableTrendDiff;
    }

    public function getStableTrendDiff(): float
    {
        return $this->stableTrendDiff;
    }

    public function updateFromFormData(?int $id, array $formData)
    {
        if ($id) {
            $sensor = $this->sensorRepository->find($id);
        } else {
            $sensor = new Sensor();
        }

        if ($formData['enabledQuantities']) {
            $formData['enabledQuantities'] = array_map(function ($item) {
                return $this->qs->transformQuantityId($item);
            }, $formData['enabledQuantities']);
        }

        $sensor->setFormDataProperties($formData);

        $this->sensorRepository->add($sensor, true);
    }

    public function writeSensorData(string $id, array $data): void
    {
        $findSensor = $this->sensorRepository->getSensorWithEnabledQuantities(
            $id
        );

        if ($findSensor) {
            $enabledQuantities = $findSensor->getEnabledQuantities()->toArray();

            foreach ($enabledQuantities as $quantity) {
                $quantityName = $quantity->getParameterName();
                if (array_key_exists($quantityName, $data)) {
                    $sensorData = SensorDataFactory::createSensorData(
                        $findSensor
                    )
                        ->setQuantity($quantity)
                        ->setValue((float) $data[$quantityName])
                        ->setDatetime(TimeService::getDateTimeWithoutSeconds());
                    $this->sensorDataRepository->add($sensorData, true);
                }
            }
        }
    }

    public function getSensorsWithLastTemperature(): array
    {
        $sensors = $this->sensorRepository->getSensorsWithLastData();

        $sensorsMap = array_map(function ($item) {
            return [
                'sensor' => $item[0]->toArray(),
                'value' => $item['value'],
                'unit' => $item['unit'],
            ];
        }, $sensors);

        return $sensorsMap;
    }

    public function getSensorWithData($id, SensorFilterQuery $dataQuery): array
    {
        try {
            $sensor = $this->sensorRepository->find($id);

            if (!$sensor) {
                throw new SensorNotFoundException($id);
            }

            $enabledQuantities = $sensor->getEnabledQuantities();

            foreach ($enabledQuantities as $quantity) {
                $sensorData = $this->sensorDataRepository->getSensorData(
                    $dataQuery,
                    $sensor,
                    $quantity
                );
                $toRet[] = $sensorData;
            }

            return array_merge(...$toRet);
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    public function getLastDataWithTrendValues(&$data)
    {
        $ret = [];

        // get first value of each quantity -> it is current value
        foreach ($data as $key => $sensorData) {
            $quantity = $sensorData->getQuantity();

            if ($quantity !== null) {
                $quantityId = $quantity->getId();

                if (!isset($ret[$quantityId])) {
                    $ret[$quantityId] = [
                        'quantity' => $quantity,
                        'current' => $sensorData->getValue(),
                        'trendValues' => [],
                    ];

                    unset($ret[$key]);
                } else {
                    $ret[$quantityId][
                        'trendValues'
                    ][] = $sensorData->getValue();
                }
            }
        }

        return $ret;
    }

    /**
     * Obtain trend by comparing the current value with the average of previous values
     *
     * @param array $data
     * @param [type] $currentValue
     * @return Trend
     */
    public function calculateTrend(array $data, $currentValue): string
    {
        $average = array_sum($data) / count($data);
        $diff = $currentValue - $average;

        if (abs($diff) <= $this->stableTrendDiff) {
            return Trend::STABLE;
        }

        if ($diff > 0) {
            return Trend::RISING;
        } else {
            return Trend::FAILING;
        }
    }

    public function calculateDewPoint($temperature, $humidity)
    {
        return 
            pow($humidity / 100, 0.125) * (112 + 0.9 * $temperature) + 0.1 * $temperature -112;
    }
}

?>
