<?php 

namespace App\Service;
use App\Entity\Quantity;
use App\Exceptions\QuantityExistsException;
use App\Repository\QuantityRepository;
use Doctrine\ORM\EntityManagerInterface;

class QuantityService {

    /** @var EntityManagerInterface */
    private $em;

    /** @var QuantityRepository */
    private $qr;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        $this->qr = $em->getRepository(Quantity::class);
    }

    public function add(Quantity $quantity) {
        
        $exists = $this->qr->findOneBy(["name"=> $quantity->getName()]);
        if ($exists) {
            throw new QuantityExistsException();
        }

        $this->em->persist($quantity);
        $this->em->flush();
    }

    public function update(Quantity $quantity) {
        

        $this->em->persist($quantity);
        $this->em->flush();
    }

    public function transformQuantityId(int $qId) : Quantity {
        $quantity = $this->qr->find($qId);

        if (!$quantity) {
            throw new QuantityNotFoundException();
        }

        return $quantity;
    }

}

?>