<?php 

namespace App\Service;

class AppService {

    private $title;

    public function __construct(string $title) {
        $this->title = $title;
    }

    public function getTitle():?string
    {
        return $this->title;
    }

}

?>