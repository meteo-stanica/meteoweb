<?php

namespace App\Service;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /** @var UserPasswordEncoderInterface */
    private $encoder;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EntityManagerInterface $em
    ) {
        $this->encoder = $encoder;
        $this->em = $em;
    }

    public function createAdmin($env)
    {
        $admin = new User();
        $admin
            ->setRoles(['ROLE_ADMIN'])
            ->setEmail('root@localhost')
            ->setPassword(
                $this->encoder->encodePassword(
                    $admin,
                    $env == 'prod' ? 'veHu3pNN994uv04' : 'root'
                )
            );

        $this->em->persist($admin);
        $this->em->flush();
    }
}

?>
