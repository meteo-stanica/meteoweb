<?php 

namespace App\Service;

class TimeService {


    public static function getDateTimeWithoutSeconds()
    {
        $date = new \DateTimeImmutable();

        return $date->setTime($date->format('H'), $date->format('i'), 0);
    }

}

?>