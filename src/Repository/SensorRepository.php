<?php

namespace App\Repository;

use App\Entity\Quantity;
use App\Repository\SensorDataRepository;
use App\Entity\Sensor;
use App\Entity\SensorData;
use App\Exceptions\SensorNotFoundException;
use App\Filters\SensorFilterQuery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Sensor>
 *
 * @method Sensor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sensor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sensor[]    findAll()
 * @method Sensor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SensorRepository extends ServiceEntityRepository
{
    /**
     *
     * @var SensorDataRepository
     */
    private $sensorDataRepository;

    /**
     *
     * @var QuantityRepository
     */
    private $quantityRepository;

    public function __construct(
        ManagerRegistry $registry,
        $sensorDataRepository,
        $quantityRepository
    ) {
        parent::__construct($registry, Sensor::class);
        $this->sensorDataRepository = $sensorDataRepository;
        $this->quantityRepository = $quantityRepository;
    }

    public function add(Sensor $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Sensor $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getSensorWithEnabledQuantities(string $id): ?Sensor
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from(Sensor::class, 's')
            ->where('s.sensorId = :sensorId')
            ->andWhere('s.enabled = :enabled')
            ->setParameters(['sensorId' => $id, 'enabled' => true])
            ->setMaxResults(1);

        $sensor = $qb->getQuery()->getOneOrNullResult();

        if (!$sensor) {
            throw new SensorNotFoundException($id);
        }

        return $sensor;
    }

    public function getSensorLastDataTime(string $quantity, $dql = false)
    {
        $query = $this->createQueryBuilder('s')
            ->select('MAX(sd.datetime)')
            ->join('s.sensorData', 'sd')
            ->join('sd.quantity', 'q')
            ->andWhere('q.parameterName = :quantity')
            ->setMaxResults(1)
            ->setParameter('quantity', $quantity)
        ;

        return $dql ? $query->getQuery()->getDQL() : $query->getQuery()->getSingleScalarResult();
    }

    public function getSensorsWithLastData()
    {
        $subquery = $this->getSensorLastDataTime('temperature', true);

        $qb = $this->createQueryBuilder('s')
            ->addSelect('sd.value,q.unit')
            ->leftJoin('s.sensorData', 'sd')
            ->leftJoin('sd.quantity', 'q')
            ->where('s.enabled = :enabled')
            ->andWhere('q.parameterName = :temperature')
            ->andWhere(`sd.datetime = ($subquery)`)
            ->setParameters([
                'enabled' => true,
                'temperature' => 'temperature',
            ]);

        return $qb->getQuery()->getResult();
    }

    public function getSensors(): array
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s, eq, sd')
            ->from(Sensor::class, 's')
            ->leftJoin('s.enabledQuantities', 'eq')
            ->leftJoin('s.sensorData', 'sd')
            ->andWhere('s.enabled=true');

        return $qb->getQuery()->getResult();
    }

    public function getSensorPreviousDaysData(int $id)
    {
        $maxDatetimeFromQuery = $this->createQueryBuilder('s')
            ->select('MAX(sd.datetime)')
            ->join('s.sensorData', 'sd')
            ->where('s.id = :sensorId')
            ->setParameter('sensorId', $id)
            ->getQuery()
            ->getSingleScalarResult();

        $maxDatetime = new \DateTimeImmutable($maxDatetimeFromQuery);

        $yesterday = clone $maxDatetime->modify('-1 day');
        $twoDaysBefore = clone $maxDatetime->modify('-2 day');
        $threeDaysBefore = clone $maxDatetime->modify('-3 day');
        $yearBefore = clone $maxDatetime->modify('1 year');

        $qb = $this->createQueryBuilder('s');
        $qb
            ->addSelect(['sd.value, sd.datetime'])
            ->leftJoin('s.sensorData', 'sd')
            ->leftJoin('sd.quantity', 'q')
            ->where('s.enabled = :enabled')
            // ->andWhere('q.parameterName = :temperature')
            ->andWhere($qb->expr()->in('sd.datetime', ':datetimes'))
            ->setParameters([
                'enabled' => true,
                // 'temperature' => 'temperature',
                'datetimes' => [
                    $yesterday->format('Y-m-d H:i:s'),
                    $twoDaysBefore->format('Y-m-d H:i:s'),
                    $threeDaysBefore->format('Y-m-d H:i:s'),
                    $yearBefore->format('Y-m-d H:i:s'),
                ],
            ]);

        dump($qb->getQuery()->getResult());

        exit();

        return $maxDatetime;
    }
}
