<?php

namespace App\Repository;

use App\Entity\Quantity;
use App\Entity\Sensor;
use App\Entity\SensorData;
use App\Filters\SensorFilterQuery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SensorData>
 *
 * @method SensorData|null find($id, $lockMode = null, $lockVersion = null)
 * @method SensorData|null findOneBy(array $criteria, array $orderBy = null)
 * @method SensorData[]    findAll()
 * @method SensorData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SensorDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SensorData::class);
    }

    public function add(SensorData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SensorData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getSensorData(
        ?SensorFilterQuery $sensorFilterQuery,
        ?Sensor $sensor,
        ?Quantity $quantity
    ) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('sd')->from(SensorData::class, 'sd');

        if ($sensor) {
            $qb
                ->andWhere('sd.sensorId = :sensor')
                ->setParameter('sensor', $sensor);
        }

        if ($quantity) {
            $qb
                ->andWhere('sd.quantity = :quantity')
                ->setParameter('quantity', $quantity);
        }

        if ($sensorFilterQuery && $sensorFilterQuery->getDatetimeFrom()) {
            $qb->andWhere('sd.datetime >= :datetimeFrom');
            $qb->setParameter(
                'datetimeFrom',
                $sensorFilterQuery->getDatetimeFrom()
            );
        }

        if ($sensorFilterQuery && $sensorFilterQuery->getDatetimeTo()) {
            $qb->andWhere('sd.datetime <= :datetimeTo');
            $qb->setParameter(
                'datetimeTo',
                $sensorFilterQuery->getDatetimeTo()
            );
        }

        if ($sensorFilterQuery && $sensorFilterQuery->getCount()) {
            $qb->setMaxResults($sensorFilterQuery->getCount());
        }

        if ($sensorFilterQuery && $sensorFilterQuery->getOffset()) {
            $qb->setFirstResult($sensorFilterQuery->getOffset());
        }

        return $qb->getQuery()->getResult();
    }

    public function findSensorDataByDatetime(
        int $sensorId,
        \DateTimeInterface $datetime,
        $quantities = null
    ) {
        $qb = $this->createQueryBuilder('sd')
            ->join('sd.sensorId', 's')
            ->leftJoin('sd.quantity', 'q')
            ->where('sd.sensorId = :sensorId')
            ->andWhere('sd.datetime = :datetime')

            ->setParameter('sensorId', $sensorId)
            ->setParameter('datetime', $datetime);

        if (isset($quantities)) {
            $qb
                ->andWhere('q.parameterName IN (:quantities)')
                ->setParameter('quantities', $quantities);
        }

        return $qb->getQuery()->getResult();
    }

    public function getSensorDataCount(int $sensorId): int
    {
        $qb = $this->createQueryBuilder('sd')
            ->select('COUNT (DISTINCT sd.datetime)')
            ->join('sd.sensorId', 's')
            ->leftJoin('sd.quantity', 'q')
            ->where('sd.sensorId = :sensorId')
            ->setParameter('sensorId', $sensorId);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getSensorMinMaxQuantityValue(
        int $sensorId,
        string $parameterName,
        $minmax
    ) {
        $qb = $this->createQueryBuilder('sd')
            ->select(
                'sd.datetime, q.parameterName, q.unit'
            );
            
            $selectFunction = ($minmax == 'MIN') ? 'MIN' : 'MAX';
            $qb->addSelect($selectFunction . '(sd.value) AS value');
            
            $qb->join('sd.sensorId', 's')
            ->leftJoin('sd.quantity', 'q')
            ->where('sd.sensorId = :sensorId')
            ->setParameter('sensorId', $sensorId)
            ->andWhere('q.parameterName = :parameter')
            ->setParameter('parameter', $parameterName)
            ->groupBy('sd.datetime, q.unit')
            //->orderBy('value', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    //    /**
    //     * @return SensorData[] Returns an array of SensorData objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?SensorData
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
