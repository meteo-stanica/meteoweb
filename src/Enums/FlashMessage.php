<?php 

namespace App\Enums;

class FlashMessage {
    const SUCCESS = "success";
    const INFO = "info";
    const WARNING = "warning";
    const ERROR = "error";
}

?>