<?php 

namespace App\Response;
use App\Entity\Quantity;

class QuantityCurrentValueWithResponse implements Response {

    /**
     * @var Quantity
     */
    private $quantity;

    private $current;

    private $trendValues;

    private $trend;

    /**
     * Get the value of quantity
     */
    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     */
    public function setQuantity(Quantity $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get the value of current
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Set the value of current
     */
    public function setCurrent($current): self
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get the value of trendValues
     */
    public function getTrendValues()
    {
        return $this->trendValues;
    }

    /**
     * Set the value of trendValues
     */
    public function setTrendValues(array $trendValues): self
    {
        $this->trendValues = $trendValues;

        return $this;
    }

    /**
     * Get the value of trend
     */
    public function getTrend():string
    {
        return $this->trend;
    }

    /**
     * Set the value of trend
     */
    public function setTrend(string $trend): self
    {
        $this->trend = $trend;

        return $this;
    }

    public function toArray(): array {
        return [
            'quantity' => $this->quantity->toArray(),
            'current' => $this->current,
            'trendValues' => $this->trendValues,
            'trend' => $this->trend
        ];
    }
}

?>