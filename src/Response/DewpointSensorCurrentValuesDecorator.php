<?php

namespace App\Response;
use App\Service\SensorService;

// This will add dewpoint to response
class DewpointSensorCurrentValuesDecorator extends
    SensorCurrentValueWithTrendResponse
{
    /**
     *
     * @var SensorResponse
     */
    private $sensorResponse;

    /**
     *
     * @var SensorService
     */
    private $sensorService;

    public function __construct(
        SensorCurrentValueWithTrendResponse $sensorResponse,
        SensorService $sensorService
    ) {
        $this->sensorResponse = $sensorResponse;
        $this->sensorService = $sensorService;
    }

    public function toArray()
    {
        $tempAndHumid = $this->getTemperatureAndHumidity();

        $actual = $this->sensorResponse->toArray();


        if (isset($tempAndHumid['temperature']) && isset($tempAndHumid['humidity'])) {
            $dewpoint = $this->getDewPoint(
                $tempAndHumid['temperature'],
                $tempAndHumid['humidity']
            );

            $actual[] = [
                'quantity' => [
                    'name' => 'Rosný bod',
                    'parameterName' => 'dewpoint',
                    'unit' => '°C',
                ],
                'current' => round($dewpoint,2),
            ];
        }

        return $actual;
    }

    private function getTemperatureAndHumidity()
    {
        $temperature = null;
        $humidity = null;
        
        foreach ($this->sensorResponse->getQuantityResponses() as $quantityResponse) {
            if (
                $quantityResponse->getQuantity()->getParameterName() === 'temperature'
            ) {
                $temperature = $quantityResponse->getCurrent();
            }
            if ($quantityResponse->getQuantity()->getParameterName() === 'humidity') {
                $humidity = $quantityResponse->getCurrent();
            }
            if ($humidity && $temperature) {
                break;
            }
        }

        return [
            'temperature' => $temperature,
            'humidity' => $humidity,
        ];
    }

    private function getDewPoint($temperature, $humidity)
    {
        return $this->sensorService->calculateDewPoint($temperature, $humidity);
    }
}

?>
