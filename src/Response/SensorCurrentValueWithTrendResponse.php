<?php 

namespace App\Response;

class SensorCurrentValueWithTrendResponse implements Response {

    private $quantityResponses = [];

    public function addQuantityResponse(QuantityCurrentValueWithResponse $quantityValues): self
    {
        $this->quantityResponses[] = $quantityValues;

        return $this;
    }

    public function getQuantityResponses() {
        return $this->quantityResponses;
    }

    public function toArray() {
        return array_map(function($item){
            return $item->toArray();
        },$this->quantityResponses);
    }
}

?>