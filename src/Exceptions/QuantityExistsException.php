<?php 

namespace App\Exceptions;

class QuantityExistsException extends \Exception {
    
    public function __construct($message =  'Quantity already exists')
    {
        parent::__construct($message);
    }

}

?>