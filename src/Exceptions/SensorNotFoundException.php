<?php 

namespace App\Exceptions;

class SensorNotFoundException extends \Exception {
    
    public function __construct(string $id)
    {
        $message = sprintf('Sensor with id %s not exists', $id);
        parent::__construct($message);
    }
}

?>