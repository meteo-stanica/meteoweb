<?php 

namespace App\Factory;
use App\Entity\Sensor;
use App\Entity\SensorData;

class SensorDataFactory {

    public static function createSensorData(Sensor $sensor):SensorData
    {
        $sensorData = new SensorData;
        $sensorData->setSensorId($sensor);

        return $sensorData;
    }

}

?>