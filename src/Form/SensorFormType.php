<?php

namespace App\Form;

use App\Entity\Quantity;
use App\Entity\Sensor;
use App\Repository\QuantityRepository;
use App\Repository\SensorRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSetDataEvent;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SensorFormType extends AbstractType
{

    /**
     * @var QuantityRepository
     */
    private $quantityRepository;

    public function __construct(QuantityRepository $quantityRepository)
    {
        $this->quantityRepository = $quantityRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('sensorId', TextType::class, ['label' => 'Senzor ID'])
            ->add('name', TextType::class, ['label' => 'Názov'])
            ->add('enabled', CheckboxType::class, ['label' => 'Povolené'])
            ->add('enabledQuantities', ChoiceType::class, [
                'choices' => $this->quantityRepository->getAllEnabledArray(),
                'label' => 'Povolené veličiny',
                'expanded' => true,
                'multiple' => true,
                // 'data' => [3],
                'mapped' => false,
            ]
        );

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (PostSetDataEvent $event) {
            // @var App/Entity/Sensor
            $sensor = $event->getData();
            $form = $event->getForm();

            if ($sensor->getId()){
                $enabled = array_map(function(Quantity $item){
                    return $item->getId();
                }, $sensor->getEnabledQuantities()->toArray());
                $form->get('enabledQuantities')->setData($enabled);
            }         

            $label = $sensor->getId() ? "Uložiť senzor" : "Pridať senzor";
            $form->add("save", SubmitType::class, ['label' => $label]);
        });

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Sensor::class,
        ]);
    }
}
