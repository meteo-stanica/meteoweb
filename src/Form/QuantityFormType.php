<?php

namespace App\Form;

use App\Entity\Quantity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuantityFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('name', TextType::class, ['label'=> 'Názov'])
            ->add('unit', TextType::class, ['label' => 'Jednotka'])
            ->add('enabled', CheckboxType::class, ['label' => 'Povolené'])
            ->add('parameterName', TextType::class, ['label'=>'Názov parametru'])
            // ->add('save', SubmitType::class, ['label' => 'Pridať veličinu'])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (PreSetDataEvent $event) {
            // @var App/Entity/Quantity
            $quantity = $event->getData();
            $form = $event->getForm();

            //$form->add('id', HiddenType::class, ['label'=> 'id', 'mapped' => $quantity->getId() ? true : false]);
            $label = $quantity->getId() ? "Uložiť veličinu" : "Pridať veličinu";
            $form->add("save", SubmitType::class, ['label' => $label]);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Quantity::class,
        ]);
    }
}
