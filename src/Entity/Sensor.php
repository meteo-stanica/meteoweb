<?php

namespace App\Entity;

use App\Repository\SensorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SensorRepository::class)
 */
class Sensor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $sensorId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity=SensorData::class, mappedBy="sensorId", orphanRemoval=true, fetch="EAGER")
     */
    private $sensorData;

    /**
     * @ORM\ManyToMany(targetEntity=Quantity::class, inversedBy="sensors")
     */
    private $enabledQuantities;

    /**
     * @ORM\OneToOne(targetEntity=SensorLocation::class, mappedBy="sensor", cascade={"persist", "remove"})
     */
    private $sensorLocation;

    public function __construct()
    {
        $this->value = new ArrayCollection();
        $this->enabledQuantities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSensorId(): ?string
    {
        return $this->sensorId;
    }

    public function setSensorId(string $sensorId): self
    {
        $this->sensorId = $sensorId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection<int, SensorData>
     */
    public function getValue(): Collection
    {
        return $this->value;
    }

    public function addValue(SensorData $value): self
    {
        if (!$this->value->contains($value)) {
            $this->value[] = $value;
            $value->setSensorId($this);
        }

        return $this;
    }

    public function removeValue(SensorData $value): self
    {
        if ($this->value->removeElement($value)) {
            // set the owning side to null (unless already changed)
            if ($value->getSensorId() === $this) {
                $value->setSensorId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Quantity>
     */
    public function getEnabledQuantities(): Collection
    {
        return $this->enabledQuantities;
    }

    public function addEnabledQuantity(Quantity $enabledQuantity): self
    {
        if (!$this->enabledQuantities->contains($enabledQuantity)) {
            $this->enabledQuantities[] = $enabledQuantity;
        }

        return $this;
    }

    public function removeEnabledQuantity(Quantity $enabledQuantity): self
    {
        $this->enabledQuantities->removeElement($enabledQuantity);

        return $this;
    }

    public function setFormDataProperties(array $formData)
    {
        $reflection = new \ReflectionClass($this);
        $formData = array_filter(
            $formData,
            function ($property) use ($reflection) {
                return $reflection->hasProperty($property);
            },
            ARRAY_FILTER_USE_KEY
        );

        foreach ($formData as $key => $item) {
            $this->{$key} = $item;
        }

        return $this;
    }

    public function getLastData()
    {
        return $this->value;
    }

    public function toArray()
    {
        return [
            'sensorId' => $this->sensorId,
            'name' => $this->name,
            'enabled' => $this->enabled
        ];
    }

    /**
     * Get the value of sensorData
     */
    public function getSensorData()
    {
        return $this->sensorData;
    }

    /**
     * Set the value of sensorData
     */
    public function setSensorData($sensorData): self
    {
        $this->sensorData = $sensorData;

        return $this;
    }

    public function getSensorLocation(): ?SensorLocation
    {
        return $this->sensorLocation;
    }

    public function setSensorLocation(?SensorLocation $sensorLocation): self
    {
        // unset the owning side of the relation if necessary
        if ($sensorLocation === null && $this->sensorLocation !== null) {
            $this->sensorLocation->setSensor(null);
        }

        // set the owning side of the relation if necessary
        if ($sensorLocation !== null && $sensorLocation->getSensor() !== $this) {
            $sensorLocation->setSensor($this);
        }

        $this->sensorLocation = $sensorLocation;

        return $this;
    }
}
