<?php

namespace App\Entity;

use App\Repository\QuantityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuantityRepository::class)
 */
class Quantity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=155)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $unit;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=155)
     */
    private $parameterName;

    /**
     * @ORM\ManyToMany(targetEntity=Sensor::class, mappedBy="enabledQuantities")
     */
    private $sensors;

    /**
     * @ORM\OneToMany(targetEntity=SensorData::class, mappedBy="quantity")
     */
    private $sensorData;

    public function __construct()
    {
        $this->sensors = new ArrayCollection();
        $this->sensorData = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getParameterName(): ?string
    {
        return $this->parameterName;
    }

    public function setParameterName(string $parameterName): self
    {
        $this->parameterName = $parameterName;

        return $this;
    }

    /**
     * @return Collection<int, Sensor>
     */
    public function getSensors(): Collection
    {
        return $this->sensors;
    }

    public function addSensor(Sensor $sensor): self
    {
        if (!$this->sensors->contains($sensor)) {
            $this->sensors[] = $sensor;
            $sensor->addEnabledQuantity($this);
        }

        return $this;
    }

    public function removeSensor(Sensor $sensor): self
    {
        if ($this->sensors->removeElement($sensor)) {
            $sensor->removeEnabledQuantity($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, SensorData>
     */
    public function getSensorData(): Collection
    {
        return $this->sensorData;
    }

    public function addSensorData(SensorData $sensorData): self
    {
        if (!$this->sensorData->contains($sensorData)) {
            $this->sensorData[] = $sensorData;
            $sensorData->setQuantity($this);
        }

        return $this;
    }

    public function removeSensorData(SensorData $sensorData): self
    {
        if ($this->sensorData->removeElement($sensorData)) {
            // set the owning side to null (unless already changed)
            if ($sensorData->getQuantity() === $this) {
                $sensorData->setQuantity(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'unit' => $this->unit,
            'parameterName' => $this->parameterName
        ];
    }
}
