<?php

namespace App\Entity;

use App\Repository\SensorDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SensorDataRepository::class)
 */
class SensorData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Sensor::class, inversedBy="sensorData")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sensorId;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity=Quantity::class, inversedBy="sensorData")
     */
    private $quantity;

    /**
     * set sensor to data
     *
     * @param Sensor $sensor
     */

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSensorId(): ?Sensor
    {
        return $this->sensorId;
    }

    public function setSensorId(?Sensor $sensorId): self
    {
        $this->sensorId = $sensorId;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getQuantity(): ?Quantity
    {
        return $this->quantity;
    }

    public function setQuantity(?Quantity $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
