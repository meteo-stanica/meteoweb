<?php 

namespace App\Filters;

class SensorFilterQuery {

    private $count;

    /**
     * @var \DateTimeInterface
     */
    private $datetimeFrom;

    /**
     * @var \DateTimeInterface
     */
    private $datetimeTo;

    private $offset;


    /**
     * Get the value of count
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * Set the value of count
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get the value of datetimeFrom
     */
    public function getDatetimeFrom(): ?\DateTimeInterface
    {
        return $this->datetimeFrom;
    }

    /**
     * Set the value of datetimeFrom
     */
    public function setDatetimeFrom(?\DateTimeInterface $datetimeFrom): self
    {
        $this->datetimeFrom = $datetimeFrom;

        return $this;
    }

    /**
     * Get the value of datetimeTo
     */
    public function getDatetimeTo(): ?\DateTimeInterface
    {
        return $this->datetimeTo;
    }

    /**
     * Set the value of datetimeTo
     */
    public function setDatetimeTo(?\DateTimeInterface $datetimeTo): self
    {
        $this->datetimeTo = $datetimeTo;

        return $this;
    }

    /**
     * Get the value of offset
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * Set the value of offset
     */
    public function setOffset(?int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }
}

?>