<?php

namespace App\Command;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeAdminCommand extends Command
{
    protected static $defaultName = 'make:admin';
    protected static $defaultDescription = 'Make administrator';

    /** @var UserService */
    private $userService;

    public function __construct(UserService $us)
    {
        parent::__construct();
        $this->userService = $us;
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->userService->createAdmin(getenv('APP_ENV'));
            $io->success('Admin has been created');
        } catch (\Exception $e) {
            $io->error('You have a new command! Now make it your own! Pass --help to see your options.');
        }

        return Command::SUCCESS;
    }
}
