<?php 

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AssetController extends AbstractController {

    /**
     * @Route("/assets/js/routes.js", name="routes.js")
     */
    public function routesAction() {
        
        $response = new Response();
        $response->headers->set("Content-Type", "text/javascript");

        return $this->render("assets/js/routes.html.twig", []);
    }


}

?>