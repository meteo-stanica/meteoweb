<?php

namespace App\Controller\API;

use App\Repository\SensorRepository;
use App\Service\SensorService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostDataController extends AbstractController
{

    // @var SensorRepository
    private $sensorRepository;

    /**
     * @var SensorService
     */
    private $sensorService;
    private $logger;

    public function __construct(SensorRepository $sensorRepository, LoggerInterface $loggger, SensorService $sensorService)
    {
        $this->sensorRepository = $sensorRepository;
        $this->logger = $loggger; 
        $this->sensorService = $sensorService;
    }

    /**
     * @Rest\Post("/api/posts")
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     *
     * 
     * @param Request $request
     * @return array
     */
    public function postMeteoData(Request $request)
    {
        $data = $request->toArray();

        try {
            $sensor = $this->sensorRepository->getSensorWithEnabledQuantities($data['id']);
            $this->sensorService->writeSensorData($data['id'], $data);
            return new JsonResponse('ok');
        } catch (\Exception $ex){
            $ex;
        }


        return new JsonResponse(['message' => $data]);
    }

    /**
     * @Route("/api/test")
     */
    public function testMeteoData() {
        $request = [
            "id" => "4er72q845",
            "temperature" => "424252555225",
            "humidity" => "asfasf"
        ];
        $data = $request;

        try {
            $this->sensorService->writeSensorData($data['id'], $data);
            return new JsonResponse('ok');
        } catch (\Exception $ex) {
            $this->logger->error($ex->getMessage());
            return new Response($ex, 503);
        }

        
    }
}
