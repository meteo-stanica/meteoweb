<?php

namespace App\Controller\API;

use App\Filters\SensorFilterQuery;
use App\Repository\SensorDataRepository;
use App\Repository\SensorRepository;
use App\Response\DewpointSensorCurrentValuesDecorator;
use App\Response\QuantityCurrentValueWithResponse;
use App\Response\SensorCurrentValueWithTrendResponse;
use App\Service\AppService;
use App\Service\SensorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AppDataController extends AbstractController
{
    /**
     * @var AppService
     */
    private $appService;

    /**
     * SensorService
     *
     * @var SensorService
     */
    private $sensorService;

    /**
     * @var SensorDataRepository
     */
    private $sensorDataRepository;

    /**
     * @var SensorRepository
     */
    private $sensorRepository;

    public function __construct(
        AppService $appService,
        SensorService $sensorService,
        SensorDataRepository $sensorDataRepository,
        SensorRepository $sensorRepository
    ) {
        $this->appService = $appService;
        $this->sensorService = $sensorService;
        $this->sensorDataRepository = $sensorDataRepository;
        $this->sensorRepository = $sensorRepository;
    }

    /**
     * Returns app name
     *
     * @Rest\Get("/api/name", name="api_app_name")
     */
    public function name(): JsonResponse
    {
        return new JsonResponse($this->appService->getTitle());
    }

    /**
     * Return sensors list with actual temperature
     *
     * @Rest\Get("/api/sensors", name="api_sensors")
     * @return JsonResponse
     */
    public function sensorsList(): JsonResponse
    {
        $sensors = $this->sensorService->getSensorsWithLastTemperature();

        return new JsonResponse($sensors);
    }

    /**
     * Return sensors with current data and trend
     *
     * @Rest\View
     * @Rest\Get("/api/sensor_actual/{id}", name="api_sensor_actual", requirements={"id" = "\d+"}, defaults={"id" = 1})
     * @return JsonResponse
     */
    public function sensorsActual($id): JsonResponse
    {
        $filter = new SensorFilterQuery();
        $filter->setCount(4);

        try {
            $sensor = $this->sensorService->getSensorWithData($id, $filter);

            $groupedSensorValues = $this->sensorService->getLastDataWithTrendValues(
                $sensor
            );

            $sensorValuesResponse = new SensorCurrentValueWithTrendResponse();

            $quantities = [];

            foreach ($groupedSensorValues as &$values) {
                $trendValues = $values['trendValues'];
                $currentValue = $values['current'];
                $response = new QuantityCurrentValueWithResponse();
                $response
                    ->setQuantity($values['quantity'])
                    ->setCurrent($currentValue)
                    ->setTrendValues($trendValues)
                    ->setTrend(
                        $this->sensorService->calculateTrend(
                            $trendValues,
                            $currentValue
                        )
                    );
                $sensorValuesResponse->addQuantityResponse($response);

                $quantities[] = $response->getQuantity()->getParameterName();
            }

            //if sensor contains temperatura and humidity, calculate also dewpoint
            if (
                in_array('temperature', $quantities) &&
                in_array('humidity', $quantities)
            ) {
                $sensorValuesResponse = new DewpointSensorCurrentValuesDecorator(
                    $sensorValuesResponse,
                    $this->sensorService
                );
            }

            return new JsonResponse($sensorValuesResponse->toArray());
        } catch (\Exception $e) {
            //TODO Error Response
            return new JsonResponse($e->getMessage());
        }
    }

    /**
     * Return sensor with previous dates but same time data
     *
     * @Rest\View
     * @Rest\Get("/api/sensor_previous_dates/{id}", name="api_sensor_previous_dates", requirements={"id" = "\d+"}, defaults={"id" = 1})
     * @return JsonResponse
     */
    public function sensorPreviousDaysData($id): JsonResponse
    {
        $maxDatetime = $this->sensorRepository->getSensorLastDataTime(
            'temperature'
        );

        $maxDatetimeAsDate = new \DateTimeImmutable($maxDatetime);

        $quantities = ['temperature'];

        $dateIntervals = ['-1 day', '-2 day', '-3 day', '-1 year'];

        $ret = [];
        foreach ($dateIntervals as $interval) {
            $dateTime = clone $maxDatetimeAsDate->modify($interval);
            $sensorData = $this->sensorDataRepository->findSensorDataByDatetime(
                $id,
                $dateTime,
                $quantities
            );

            foreach ($sensorData as $data) {
                $ret[$data->getDatetime()->format('d. n. Y')][] = sprintf(
                    '%d %s',
                    $data->getValue(),
                    $data->getQuantity()->getUnit()
                );
            }
        }

        return new JsonResponse($ret);
    }

    /**
     * Return sensor statistics
     *
     * @Rest\View
     * @Rest\Get("/api/sensor_stats/{id}", name="api_sensor_stats", requirements={"id" = "\d+"}, defaults={"id" = 1})
     * @return JsonResponse
     */
    public function sensorStatistics($id)
    {
        $counts = $this->sensorDataRepository->getSensorDataCount($id);
        $max = $this->sensorDataRepository->getSensorMinMaxQuantityValue(
            $id,
            'temperature',
            'MAX'
        );

        $maxString = sprintf(
            '%s %s %s',
            $max['datetime']->format('d.n.Y'),
            $max['value'],
            $max['unit']
        );


        $min = $this->sensorDataRepository->getSensorMinMaxQuantityValue(
            $id,
            'temperature',
            'MIN'
        );

        $minString = sprintf(
            '%s %s %s',
            $min['datetime']->format('d.n.Y'),
            $min['value'],
            $min['unit']
        );

        $ret = [
            'counts' => $counts,
            'max' => $maxString,
            'min' => $minString,
        ];

        return new JsonResponse($ret);
    }
}
