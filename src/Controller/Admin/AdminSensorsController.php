<?php

namespace App\Controller\Admin;

use App\Entity\Sensor;
use App\Enums\FlashMessage;
use App\Form\SensorFormType;
use App\Repository\SensorRepository;
use App\Service\SensorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSensorsController extends AbstractController
{
    //@var SensorRepository
    private $sensorRepository;

    // @var SensorService
    private $sensorService;

    public function __construct(SensorRepository $sensorRepository, SensorService $sensorService){
        $this->sensorRepository = $sensorRepository;
        $this->sensorService = $sensorService;
    }

    /**
     * @Route("/admin/sensors", name="admin_sensors")
     */
    public function index(Request $request): Response
    {

        $sensor = new Sensor();
        $addForm = $this->createForm(SensorFormType::class, $sensor);
        
        $addForm->handleRequest($request);
        if ($addForm->isSubmitted() && $addForm->isValid()) {
            $this->processForm($request);
        }

        return $this->render('admin/admin_sensors.html.twig', [
            'form' => $addForm->createView(),
            'sensors' => $this->sensorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/sensors/edit/{id}", name="admin_sensors_edit")
     */
    public function edit(int $id, Request $request) 
    {
        $sensor = $this->sensorRepository->find($id);

        if (!$sensor) {
            throw new \Exception("Sensor not found");
        }

        $updateForm = $this->createForm(SensorFormType::class, $sensor, [
            'action' => $this->generateUrl('admin_sensors_edit', 
            ['id'=> $id]),
            'csrf_protection' => false
        ]);

        $updateForm->handleRequest($request);

        if ($updateForm->isSubmitted() && $updateForm->isValid()) {
            $this->processForm($sensor, $request);
            return $this->redirectToRoute('admin_sensors');
        }

        return $this->render('form/sensor.html.twig', [
            'form' => $updateForm->createView(),
        ]);
    }

    private function processForm(Sensor $sensor, Request $request): void {
        try {
            $data = $request->request->all('sensor_form');
            $this->sensorService->updateFromFormData($sensor->getId(), $data);
            $this->addFlash(FlashMessage::SUCCESS, $sensor->getId() ? 'Sensor has been updated' : 'Sesnor has been created');
        } catch (\Exception $ex){
            $this->addFlash(FlashMessage::ERROR, $ex->getMessage());
        }
    }
}
