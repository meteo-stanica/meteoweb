<?php

namespace App\Controller\Admin;

use App\Entity\Quantity;
use App\Enums\FlashMessage;
use App\Enums\FormMode;
use App\Form\QuantityFormType;
use App\Repository\QuantityRepository;
use App\Service\QuantityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminQuantitiesController extends AbstractController
{
    // @var QuantityRepository
    private $qr;

    // @var QuantityService
    private $qs;

    public function __construct(QuantityRepository $qr, QuantityService $qs)
    {
        $this->qr = $qr;
        $this->qs = $qs;
    }

    /**
     * @Route("/admin/quantities", name="admin_quantities")
     */
    public function index(Request $request): Response
    {
        $quantities = $this->qr->findAll();

        $quantity = new Quantity();
        $addForm = $this->createForm(QuantityFormType::class, $quantity, [
            'csrf_protection' => false,
        ]);
        $addForm->handleRequest($request);

        if ($addForm->isSubmitted() && $addForm->isValid()) {
            $this->processForm($quantity);
            return $this->redirectToRoute('admin_quantities');
        }

        return $this->render('admin/admin_quantities.html.twig', [
            'quantities' => $quantities,
            'form' => $addForm->createView(),
            'modalId' => 'default-modal',
        ]);
    }

    /**
     * @Route("/admin/quantities/edit/{id}", name="admin_quantities_edit")
     */
    public function edit(int $id, Request $request)
    {
        $quantity = $this->qr->find($id);

        if (!$quantity) {
            throw new \Exception('Quantity not found');
        }

        $updateForm = $this->createForm(QuantityFormType::class, $quantity, [
            'action' => $this->generateUrl('admin_quantities_edit', [
                'id' => $id,
            ]),
            'csrf_protection' => false,
        ]);

        $updateForm->handleRequest($request);

        if ($updateForm->isSubmitted() && $updateForm->isValid()) {
            $this->processForm($quantity);
            return $this->redirectToRoute('admin_quantities');
        }

        return $this->render('form/quantity.html.twig', [
            'form' => $updateForm->createView(),
            'modalId' => 'update-modal',
        ]);
    }

    private function processForm(Quantity $quantity)
    {
        try {
            if ($quantity->getId()) {
                $this->qs->update($quantity);
                $this->addFlash(FlashMessage::SUCCESS, 'Quantity has been updated');
            } else {
                $this->qs->add($quantity);
                $this->addFlash(FlashMessage::SUCCESS, 'Quantity has been added');
            }
            
        } catch (\Exception $ex) {
            $this->addFlash(FlashMessage::ERROR, $ex->getMessage());
        }
    }
}
