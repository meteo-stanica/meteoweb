<?php

namespace App\Controller\Admin;

use App\Repository\QuantityRepository;
use App\Repository\SensorRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AdminBaseController
{
    /**
     * @Route("/admin/dashboard", name="app_admin_dashboard")
     */
    public function index(QuantityRepository $qr, SensorRepository $sr): Response
    {
        
        $menu = $this->getParameter("dashboard_links");

        $quantities = $qr->findAll();
        $sensors = $sr->findAll();

        return $this->render('admin/admin_dashboard.html.twig', [
            'menu' => $menu
        ]);
    }
}
