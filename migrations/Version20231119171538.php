<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231119171538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sensor_data ADD quantity_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sensor_data ADD CONSTRAINT FK_801762CC7E8B4AFC FOREIGN KEY (quantity_id) REFERENCES quantity (id)');
        $this->addSql('CREATE INDEX IDX_801762CC7E8B4AFC ON sensor_data (quantity_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sensor_data DROP FOREIGN KEY FK_801762CC7E8B4AFC');
        $this->addSql('DROP INDEX IDX_801762CC7E8B4AFC ON sensor_data');
        $this->addSql('ALTER TABLE sensor_data DROP quantity_id');
    }
}
