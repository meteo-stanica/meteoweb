<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231114194720 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sensor_quantity (sensor_id INT NOT NULL, quantity_id INT NOT NULL, INDEX IDX_9E7A73FA247991F (sensor_id), INDEX IDX_9E7A73F7E8B4AFC (quantity_id), PRIMARY KEY(sensor_id, quantity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sensor_quantity ADD CONSTRAINT FK_9E7A73FA247991F FOREIGN KEY (sensor_id) REFERENCES sensor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sensor_quantity ADD CONSTRAINT FK_9E7A73F7E8B4AFC FOREIGN KEY (quantity_id) REFERENCES quantity (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE sensor_quantity');
    }
}
