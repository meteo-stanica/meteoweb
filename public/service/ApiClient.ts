import axios from "axios";

export const get = async (url, queryParams) => {
    return await axios.get(`${process.env.NEXT_PUBLIC_ENDPOINT_API}${url}`, {params:queryParams});
}

export const post = async (url, queryParams) => {
    console.log('env',(`${process.env.NEXT_PUBLIC_ENDPOINT_API}${url}`));
    return await axios.post(`${process.env.NEXT_PUBLIC_ENDPOINT_API}${url}`, queryParams);
}

const ApiClient = {
    get: get,
    post: post
}

export default ApiClient;