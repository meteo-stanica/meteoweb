var meteoweb = meteoweb || {};
meteoweb.ui = {};

// loader
meteoweb.ui.loader = function() {
    return '<div class="ui-loader-icon loader-icon js-loader-icon"><span></span></div>';
}
