import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import NavigationPanel from './components/navigationPanel';
import CurrentDataBox from './components/currentDataBox/CurrentDataBox';
import PastDatesValues from './components/pastDatesValues';
import Stats from './components/stats';
import axios from 'axios';
import ApiClient from '../../public/service/ApiClient';

const Dashboard = () => {
  const [currentValues, setCurrentValues] = useState();
  const [previousDaysValues, setPreviousDaysValues] = useState();
  const [stats, setStats] = useState();
  const [sensor, setSenzor] = useState(2);

    useEffect(() => {
      const fetchData = () => {
        //actual data
        ApiClient.get(meteoweb.routes['api_sensor_actual'] + '/' + sensor).then(
          response => {
            setCurrentValues(response.data);
          });
    
        //previous days data
        ApiClient.get(meteoweb.routes['api_sensor_previous_dates'] + '/' + sensor).then(
          response => {
            setPreviousDaysValues(response.data);
          });
    
        //stats
        ApiClient.get(meteoweb.routes['api_sensor_stats'] + '/' + sensor).then(
          response => {
            setStats(response.data);
          });
      };
    
      fetchData();
    
      const interval = setInterval(fetchData, 60000); //1 minute
    
      return () => clearInterval(interval);
    }, []);

  return (
    <>
      <NavigationPanel />
      <div className='container'>
        <div className="flex grid grid-flow-row-dense grid-cols-12 grid-rows-1 gap-4 mt-4">
          <div className='col-span-3'>
            <CurrentDataBox currentValues={currentValues} />
          </div>
          <div className='col-span-4'>
            <PastDatesValues values={previousDaysValues} />
          </div>
          <div className='col-span-5'>
            <Stats data={stats}/>
          </div>
        </div>
      </div>
    </>
  )
};

const root = ReactDOM.createRoot(document.getElementById('lift-stuff-app'));
root.render(<Dashboard />);