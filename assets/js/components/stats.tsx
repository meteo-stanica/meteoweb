import React from 'react'

interface Props {
  counts: number
  max: string 
  min: string 
}

const Stats = ({ data }: { data?: Props }) => {
  if (!data) {
    return
  }

  return (
    <div>
      <div className="w-100 rounded-t bg-emerald-700 text-white p-1 text-center">
        Štatistiky
      </div>
      <div className="flex flex-row justify-between px-16 py-1 text-white bg-black">
        <div>Počet meraní</div>
        <div>{data?.counts}</div>
      </div>
      <div className="flex flex-row justify-between px-16 py-1 text-white bg-teal-800">
        <div>Maximálna teplota</div>
        <div>{data?.max}</div>
      </div>
      <div className="flex flex-row justify-between px-16 py-1 text-white bg-black">
        <div>Minimálna teplota</div>
        <div>{data?.min}</div>
      </div>
    </div>
  )
}

export default Stats
