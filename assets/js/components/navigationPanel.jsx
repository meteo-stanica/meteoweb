import axios from "axios"
import React, { useEffect, useState } from "react"

function NavigationPanel() {

    const [title, setTitle] = useState("")

    useEffect(() => {
        axios(meteoweb.routes['api_app_name']).then(
            response => {
                setTitle(response.data);
            });
    }, [])

    return (
        <section className="panel">
            <div className="meteo-header">
                <div className="container">
                    <div className="flex-wrapper">
                        <h1 className="title">{title}</h1>
                        <div className="menu">
                            <nav>
                                <ul className="lists">
                                    <li>
                                        <a href="">doma</a>
                                    </li>
                                    <ul id="sensors">

                                    </ul>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    )

}

export default NavigationPanel