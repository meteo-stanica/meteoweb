import React from 'react'

type TemperatureData = {
  [date: string]: string[]
}

interface Props {
  values: TemperatureData
}

const PastDatesValues = ({ values }: Props) => {
  if (!values) {
    return
  }

  const valuesObj = Object.entries(values)

  return (
    <div>
      <div className="w-100 rounded-t bg-emerald-700 text-white p-1 text-center">
        V minulosti o tomto čase
      </div>
      {valuesObj.map((item, index) => {
        let date: string = item[0]
        let valuesArr: string[] = item[1]

        return (
          <div className={`flex flex-row justify-between px-16 py-1 text-white ${index % 2 === 0 ? "bg-black" : "bg-teal-800" }`} key={index}>
            <div>{date}</div>
            {Array.isArray(valuesArr) && valuesArr.map((value, idx) => (
              <div key={idx}>{value}</div>
            ))}
          </div>
        )
      })}
    </div>
  )
}

export default PastDatesValues
