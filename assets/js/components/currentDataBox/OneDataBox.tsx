import React from "react";
import useValueBackground from "../../hooks/valueBackground";
import CurrentDataBox from "./CurrentDataBox";


const valueBackground = useValueBackground();

type Props = {
  values: any;
};

const OneDataBox = ({ values }: Props) => {
  return (
    <div className="grid grid-cols-1 grid-rows-1">
      <div
        className="box"
        style={{ background: valueBackground.getColorForValue(values[0]) }}
      >
        <CurrentDataBox currentValues={values[0]} />
      </div>
    </div>
  );
};

export default OneDataBox;
