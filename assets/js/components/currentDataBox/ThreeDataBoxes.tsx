import React from "react";
import useValueBackground from "../../hooks/valueBackground";
import CurrentDataBoxContent from "./CurrentDataBoxContent";

const valueBackground = useValueBackground()

type Props = {
  values: any;
};

const ThreeDataBoxes = ({ values }: Props) => {

  if (values) {
    return (
      <div className="current-boxes grid grid-cols-2 grid-rows-2">
        <div className="box col-span-2" style={{ background: valueBackground.getColorForValue(values[0]) }}>
          <CurrentDataBoxContent content={values[0]} />
        </div>
        <div className="box" style={{ background: valueBackground.getColorForValue(values[1]) }}>
          <CurrentDataBoxContent content={values[1]} />
        </div>
        <div className="box" style={{ background: valueBackground.getColorForValue(values[2]) }}>
          <CurrentDataBoxContent content={values[2]} />
        </div>
      </div>

      
    );
  }
};

export default ThreeDataBoxes;
