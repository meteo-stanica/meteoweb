import React from "react";

import useValueBackground from "../../hooks/valueBackground";
import CurrentDataBox from "./CurrentDataBox";


const valueBackground = useValueBackground();

type Props = {
  values: any;
};

const TwoDataBoxes = ({ values }: Props) => {
  if (values) {
    return (
      <div className="grid grid-cols-1 grid-rows-2">
        <div
          className="box"
          style={{ background: valueBackground.getColorForValue(values[0]) }}
        >
          <CurrentDataBox currentValues={values[0]} />
        </div>
        <div
          className="box"
          style={{ background: valueBackground.getColorForValue(values[1]) }}
        >
          <CurrentDataBox currentValues={values[1]} />
        </div>
      </div>
    );
  }
};

export default TwoDataBoxes;
