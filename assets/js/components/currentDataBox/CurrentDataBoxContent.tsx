import { faArrowDownLong, faArrowRightLong, faArrowUp, faArrowUpLong } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

const CurrentDataBoxContent = ({ content }: any) => {
  return (
    <>
      <div className="flex">
        <div className="flex flex-col justify-center items-center">
          <small>{content?.quantity?.name}</small>
          <p>
            {content?.current} {content?.quantity.unit}
          </p>
        </div>
      </div>
      {content.trend ? (<div className="trend"><FontAwesomeIcon icon={content.trend === "stable" ? faArrowRightLong : content.trend === "rising" ? faArrowUpLong : faArrowDownLong} /></div>) : ""}
    </>
  );
};

export default CurrentDataBoxContent;
