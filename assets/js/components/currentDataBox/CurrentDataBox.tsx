import React from "react";
import OneDataBox from "./OneDataBox";
import TwoDataBoxes from "./TwoDataBoxes";
import ThreeDataBoxes from "./ThreeDataBoxes";


type Props = {
  currentValues: any;
};

const CurrentDataBox = ({ currentValues }: Props) => {
  if (currentValues) {
    switch (currentValues.length) {
      case 1:
        return <OneDataBox values={currentValues} />;
        break;
      case 2:
        return <TwoDataBoxes values={currentValues} />;
        break;
      case 3:
        return <ThreeDataBoxes values={currentValues} />;
        break;
    }
  }
};

export default CurrentDataBox;
