import React from "react";

function useValueBackground() {
  const defaultColor = "#808080";

  const colorMappings: { [key: string]: { [key: number]: string } } = {
    temperature: {
      "-20": "#0c57a4", // -20°C
      "-18": "#0863ae",
      "-16": "#096fb8",
      "-14": "#107cc1",
      "-12": "#2694d3",
      "-10": "#32a1db",
      "-8": "#3fade3",
      "-6": "#4dbaeb",
      "-4": "#5bc6f2", // tu si skoncil
      "-2": "#36caef",
      "0": "#00cde9", // 0°C
      "2": "#00d0e0",
      "4": "#00d2d3",
      "6": "#00d4c2",
      "8": "#00d5af",
      "10": "#1dd59a",
      "12": "#4bd584",
      "14": "#6ad36c",
      "16": "#86d054",
      "18": "#a0cc3c",
      "20": "#b9c722", // tu
      "22": "#c1bc00",
      "24": "#c9b000",
      "26": "#d0a300",
      "28": "#d79600",
      "30": "#dd8700", // 30°C
      "32": "#e37800",
      "34": "#e86600",
      "36": "#ed5300",
      "38": "#f03a00",
      "40": "#f20d0d", // 40°C
    },
    humidity: {
      "0": "#aa3e3e", // 0% - dark red
      "10": "#b15439", // 10%
      "20": "#b56a38", // 20%
      "30": "#b67f3d", // 30%
      "40": "#b49447", // 40%
      "50": "#a19d4d", // 50%
      "60": "#8da45a", // 60%
      "70": "#79a96b", // 70%
      "80": "#53a17a", // 80%
      "90": "#2e9787", // 90%
      "100": "#108a91", // 100% - dark blue
    },
    dewpoint: {
      "0": "#1D7A8F",
    },
  };

  function getColorForValue(data: any): string {
    const type = data?.quantity?.parameterName;
    const value = data.current;
    const mappings = colorMappings[type];
    if (!mappings) {
      return defaultColor;
    }

    const keys = Object.keys(mappings)
      .map(Number)
      .sort((a, b) => b - a);
    const foundKey = keys.find((key) => value >= key);
    return foundKey ? mappings[foundKey] : defaultColor;
  }

  // test boxes
  function generateColorBoxesHTML() {
    let html: JSX.Element[] = [];

    for (const parameter in colorMappings) {
      if (Object.prototype.hasOwnProperty.call(colorMappings, parameter)) {
        let boxes: JSX.Element[] = [];

        const parameterMap = colorMappings[parameter];
        for (const key in parameterMap) {
          if (Object.prototype.hasOwnProperty.call(parameterMap, key)) {
            const color = parameterMap[key];
            boxes.push(
              <div
                className="flex w-[50px] h-[50px] m-1 p-2 justify-center text-white"
                key={key}
                style={{ backgroundColor: color }}
              >
                {key}%
              </div>
            );
          }
        }

        html.push(
          <div key={parameter}>
            <h2>{parameter}</h2>
            <div style={{ display: "flex" }}>{boxes}</div>
          </div>
        );
      }
    }

    return html;
  }

  return { getColorForValue, generateColorBoxesHTML };
}

export default useValueBackground;
